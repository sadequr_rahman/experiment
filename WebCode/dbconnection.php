<?php
	class DBConnector{
		
		private  $serverName;
		private  $userName;
		private  $password;
		private  $databaseName;
		
		public  $connectionString;
		
		public function __construct($sName,$uName,$pass,$dbName){
		
			$this->serverName=$sName;
			$this->password=$pass;
			$this->userName=$uName;
			$this->databaseName=$dbName;
		}
		
		public function __destruct(){}
		
		public function connectToDatabase(){
			
			$this -> connectionString = mysql_connect($this -> serverName,$this -> userName,$this -> password);
			if(!$this -> connectionString)
			{	
				die("Error Connecting database");
			}
			mysql_select_db($this -> databaseName,$this -> connectionString);
		}
		
		public function closeConnection(){
			
			mysql_close($this->connectionString);
		}
		
	}
	
	/*class QueryGenerator{
	
			private  $tableName;
			private  $columns;
			

			public function __construct($tName,$cols){
			
				$this->tableName=$tName;				
				$this->columns=$cols;
			}
	}
*/
?>