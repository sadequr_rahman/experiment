
#include <ESP8266WiFi.h>
extern "C"{
 #include "user_interface.h"
}



void myTimerCallback(void *pArg);

uint8_t pin = 0;
bool status = true;
os_timer_t myTimer;
uint8_t tempValue = 0, lightValue = 0, rainValue = 0, windSpeedValue=0,humidityValue=0;
char buf1[4], buf2[4],buf3[4], buf4[4], buf5[4];
uint16_t SecCount = 0;

static volatile bool SentFlag = false;

String answer = "";
const char* ssid = "Pristine Castle";
const char* password = "syeed321";

WiFiClient myClient;
String host = "192.168.0.103";
String appiKey = "123";

void myTimerCallback(void *pArg)
{
	status?digitalWrite(pin, HIGH): digitalWrite(pin, LOW);
	status?(status = false):(status = true);
	SecCount++;
	
	if (SecCount >= 60)
	{
		SecCount = 0;
		SentFlag = true;
	}
}


void user_init(void) {
	/*
	os_timer_setfn - Define a function to be called when the timer fires

	void os_timer_setfn(
	os_timer_t *pTimer,
	os_timer_func_t *pFunction,
	void *pArg)

	Define the callback function that will be called when the timer reaches zero. The pTimer parameters is a pointer to the timer control structure.

	The pFunction parameters is a pointer to the callback function.

	The pArg parameter is a value that will be passed into the called back function. The callback function should have the signature:
	void (*functionName)(void *pArg)

	The pArg parameter is the value registered with the callback function.
	*/

	os_timer_setfn(&myTimer, myTimerCallback, NULL);

	/*
	os_timer_arm -  Enable a millisecond granularity timer.

	void os_timer_arm(
	os_timer_t *pTimer,
	uint32_t milliseconds,
	bool repeat)

	Arm a timer such that is starts ticking and fires when the clock reaches zero.

	The pTimer parameter is a pointed to a timer control structure.
	The milliseconds parameter is the duration of the timer measured in milliseconds. The repeat parameter is whether or not the timer will restart once it has reached zero.

	*/

	os_timer_arm(&myTimer, 1000, true);
} // End of user_init

void setup()
{

	
	/* add setup code here */
	Serial.begin(9600);
	Serial.println("Program Init.....");
	pinMode(pin, OUTPUT);
	WiFi.begin(ssid, password);
	Serial.println("Waiting connection to the router...");
	while (WiFi.status() != WL_CONNECTED)//Print on the serial �-� while the module doesn�t connect to the network
	{
		delay(500);
		Serial.print("-");
	}
	Serial.print("\nWi-Fi connected. IP: ");
	Serial.println(WiFi.localIP());
	user_init();

}

void loop()
{

  /* add main program code here */
	if (SentFlag == true)
	{
		SentFlag = false;
		Serial.println("conneting to host....");
		
		if (myClient.connect(host.c_str(), 80))
		{
			tempValue = random(12,40);
			lightValue = random(100,255);
			rainValue = random(3,12);
			humidityValue = random(30, 100);
			windSpeedValue = random(150, 255);

			itoa(tempValue,buf1,10);
			//Serial.print("TempSensor Value is =");
			//Serial.println(buf1);
			itoa(lightValue, buf2, 10);
			//Serial.print("LightSensor Value is =");
			//Serial.println(buf2);
			itoa(rainValue, buf3 , 10);
			itoa(humidityValue, buf4, 10);
			itoa(windSpeedValue, buf5, 10);
			delay(200);
			Serial.println("connection Successful");
			Serial.println("Sending request HTTP "); // Request from the server information about the weather
			//myClient.print("GET /Test/index.php?value=\"test\"");
			myClient.print("GET /WeatherIOT/InsertData.php?apiId=123&temperature=");
			myClient.print(buf1);
			myClient.print("&rain=");
			myClient.print(buf3);
			myClient.print("&humidity=");
			myClient.print(buf4);
			myClient.print("&windSpeed=");
			myClient.print(buf5);
			myClient.print("&windDirection=");
			myClient.print(buf2);
     
			myClient.print(" ");
			myClient.print("HTTP/1.0\r\n");
			myClient.print("Host: " + host + "\r\n\r\n");
			myClient.print("Connection: close");
			delay(100);
			Serial.println("Closing Connection...");
		}
		else
		{
			Serial.println("connection failed");
		}

		while (myClient.available())//Read the answer of the API and keep the data on the string answer
		{
			char c = myClient.read();
			answer += c;
		}
		Serial.println(answer);
		answer = "";


	}


}
